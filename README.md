# README #

This README documents whatever steps are necessary for ATAC-seq analysis in Chen et al., *Neuorn* (2021)

## ATAC-seq analysis pipeline ##

### Raw reads processing ###

* Adapter trimming：trimmomatic
* FastQC after trimming
* Aligning reads: bowtie2
* Reads filtering
* Reads shifting
* Merging .bam files
* Peak calling: macs2
* Coverage for peaks in each biological replicate

### Differential Accessible Region (DRA) analysis ###

* edgeR
* RUVseq

### Contact Information ###

* Lead contact: Dr. Joseph Dougherty, jdougherty@wust.edu