#!/bin/bash 
#SBATCH --cpus-per-task=6
#SBATCH --mem=100G 
#SBATCH -J coverage 
#SBATCH --array=1-12
#SBATCH -o /working directory/logs/coverage.out 
#SBATCH -e /working directory/logs/coverage.err

module load bedtools

ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt ) 

bedtools coverage -a /working directory/results/peak_calling/all_peaks.narrowPeak \
-b /working directory/results/after_filter/${ID}.shifted.bam > /working directory/results/read_counts/${ID}.read.counts.bed
