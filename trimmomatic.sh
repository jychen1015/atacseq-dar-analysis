#!/bin/bash 
#SBATCH --cpus-per-task=6 
#SBATCH --mem=50G 
#SBATCH -J trim
#SBATCH --array=1-12
#SBATCH -o /working directory/logs/trim.out 
#SBATCH -e /working directory/logs/trim.err 

module load java 
module load trimmomatic 
ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt ) 
java -jar $TRIMMOMATIC_HOME/trimmomatic-0.38.jar PE -phred33 -threads 12 -basein /working directory/raw/${ID}_L002_R1_001.fastq.gz \
     -baseout /working directory/results/trimmed/${ID}_trimmed.fastq.gz ILLUMINACLIP:/adapter directory/NexteraPE-PE.fa:2:30:10:1:true LEADING:20 TRAILING:20 MINLEN:25

