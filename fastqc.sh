#!/bin/bash 
#SBATCH --cpus-per-task=2
#SBATCH --mem=20G
#SBATCH -J fastqc
#SBATCH --array=1-12
#SBATCH -o /working_directory/logs/fastqc_pfc.out 
#SBATCH -e /working_directory/logs/fastqc_pfc.err 

module load fastqc 
ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working_directory/lookup.txt ) 
fastqc -o /working_directory/results/fastqc_after_trim \
       -f fastq /working_directory/results/trimmed/${ID}_trimmed_1P.fastq.gz /working_directory/results/trimmed/${ID}_trimmed_2P.fastq.gz