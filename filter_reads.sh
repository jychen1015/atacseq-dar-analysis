#!/bin/bash 
#SBATCH --cpus-per-task=6
#SBATCH --mem=50G 
#SBATCH -J filter 
#SBATCH --array=1-12
#SBATCH -o /working directory/logs/filter.out 
#SBATCH -e /working director/logs/filter.err

module load samtools
module load picard

ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working director/lookup.txt ) 

samtools view -h /working director/results/post_bowtie2_align/${ID}.sorted.bam | grep -v MT | samtools view -b -o /working director/results/after_filter/${ID}.rmChrM.bam

java -jar /opt/apps/picard-tools/2.18.2/picard.jar MarkDuplicates \
I=/working director/results/after_filter/${ID}.rmChrM.bam \
O=/working director/results/after_filter/${ID}.rmChrM.rmDup.bam \
TMP_DIR=/tmp/picard-${SLURM_JOB_ID} \
M=/working director/results/after_filter/${ID}_dups.txt \
REMOVE_DUPLICATES=true

samtools view -h -b -q 10 /working director/results/after_filter/${ID}.rmChrM.rmDup.bam > /working director/results/after_filter/${ID}.rmChrM.rmDup.rmMulti.bam
samtools view -h -b -F 1804 -f 2 /working director/results/after_filter/${ID}.rmChrM.rmDup.rmMulti.bam > /working director/results/after_filter/${ID}.filtered.bam

rm /sworking director/results/after_filter/${ID}.rmChrM.bam
rm /working director/results/after_filter/${ID}.rmChrM.rmDup.bam
rm /working director/results/after_filter/${ID}.rmChrM.rmDup.rmMulti.bam